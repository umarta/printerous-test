<script>
    window.addEventListener('actResponse', event => {
        console.log(event.detail)
        Toastify({
            node: cash("#basic-non-sticky-notification-content")
                .clone()
                .removeClass("hidden")[0],
            duration: 3000,
            newWindow: true,
            text: event.detail.message,
            close: true,
            gravity: "top",
            position: "right",
            stopOnFocus: true,
        }).showToast();
        if (event.detail.isModal && event.detail.hideModal) {
            // cash('#header-footer-modal-preview').modal('hide')
            cash('#programmatically-modal').modal('hide')


        }
    })
</script>
