@section('title', 'Person')
<div>
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">
            @hasSection('title')
                @yield('title')
            @endif <!-- BEGIN: CSS Assets-->

        </h2>

    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-12">
            <!-- BEGIN: Basic Table -->
            <div class="intro-y box">
                <div class="p-5" id="basic-table">
                    <div class="preview">
                        <div class="overflow-x-auto">
                            <table class="table">
                                <thead>
                                    <tr>
                                        @foreach ($header as $head)
                                            <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">
                                                {{ $head }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $org)
                                        <tr>
                                            <td class="border-b dark:border-dark-5">{{ $org->name }}</td>
                                            <td class="border-b dark:border-dark-5">{{ $org->email }}</td>
                                            <td class="border-b dark:border-dark-5">{{ $org->role }}</td>
                                            <td class="border-b dark:border-dark-5">{{ $org->organization }}</td>
                                            <td class="border-b dark:border-dark-5">

                                                <a class="btn btn-primary mr-1 mb-2 modal-edit"
                                                    wire:click="isEdit({{ $org->id }})" data-toggle="modal"
                                                    data-target="#modal-input">
                                                    <i data-feather="edit-2" class="w-5 h-5"></i> </a>

                                                <button class="btn btn-danger mr-1 mb-2"> <i data-feather="trash"
                                                        class="w-5 h-5"></i> </button>
                                            </td>
                                        </tr>

                                    @empty
                                        <td colspan="{{ count($header) }}"
                                            class="border-b dark:border-dark-5 text-center">Data
                                            Empty</td>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="modal-input" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="font-medium text-base mr-auto">Create New Organization</h2>
                </div> <!-- END: Modal Header -->
                <div class="modal-body p-10 ">
                    <form wire:submit.prevent="saveModule(Object.fromEntries(new FormData($event.target)))"
                        id="saveModule" x-data="{ isUploading: false, progress: 0 }"
                        x-on:livewire-upload-start="isUploading = true"
                        x-on:livewire-upload-finish="isUploading = false"
                        x-on:livewire-upload-error="isUploading = false"
                        x-on:livewire-upload-progress="progress = $event.detail.progress">
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-name" class="form-label">Name</label>
                                <input id="modal-name" type="text" class="form-control" disabled name="name" wire:model="name"
                                    placeholder="Name">
                                @error('name') <span class="error">{{ $message }}</span> @enderror


                            </div>

                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-name" class="form-label">Role</label>
                                <select name="role" id="role" wire:model="role" class="form-control">
                                    <option value="0">--Polih--</option>
                                    <option value="admin">Admin</option>
                                    <option value="account_manager">Account Manager</option>
                                    <option value="user">User</option>
                                </select>
                                @error('role') <span class="error">{{ $message }}</span> @enderror
                            </div>

                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-name" class="form-label">Organization</label>
                                <select name="role" id="role" wire:model="organization_id" class="form-control">
                                    <option value="0">--Polih--</option>
                                    @foreach ($organization as $orgs)
                                        <option value="{{ $orgs->id }}">{{ $orgs->name }}</option>

                                    @endforeach
                                </select>
                                @error('role') <span class="error">{{ $message }}</span> @enderror
                            </div>



                        </div> <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer text-right"> <button type="button" data-dismiss="modal"
                                class="btn btn-outline-secondary w-20 mr-1">Cancel</button> <button type="submit"
                                class="btn btn-primary w-20">Send</button> </div> <!-- END: Modal Footer -->
                    </form>
                </div>
            </div>
        </div>
    </div> <!-- END: Modal Content -->
</div>
@section('script')
    <script src="https://code.jquery.com/jquery-3.6.0.slim.js"
        integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>

    <script>
        // Show modal

        window.addEventListener('getId', event => {
            console.log(event.detail)

            if (event.detail.isEdit) {

                cash("#header-footer-modal-preview").modal("show");
                // $('#confirm-delete-catalog').modal('show')
            }
        })
        // $('.modal-edit').click(function() {
        //     cash("#header-footer-modal-preview").modal("show");
        // })
    </script>
    @include('livewire.include.script')
@endsection
