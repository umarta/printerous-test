@section('title', 'Organization')
<div>
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">
            @hasSection('title')
                @yield('title')
            @endif <!-- BEGIN: CSS Assets-->

        </h2>
        @if ($user->role == 'admin')
            <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
                <div class="text-center"> <a href="javascript:;" data-toggle="modal"
                        data-target="#programmatically-modal" class="btn btn-primary">Create</a> </div>
                <!-- END: Modal Toggle -->

            </div>
        @endif
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-12">
            <!-- BEGIN: Basic Table -->
            <div class="intro-y box">
                <div class="p-5" id="basic-table">
                    <div class="preview">
                        <div class="overflow-x-auto">
                            <table class="table">
                                <thead>
                                    <tr>
                                        @foreach ($header as $head)
                                            <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">
                                                {{ $head }}</th>

                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $org)
                                        <tr>
                                            <td class="border-b dark:border-dark-5">
                                                <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in"
                                                    role="button" aria-expanded="false">
                                                    <img alt="Rubick Tailwind HTML Admin Template"
                                                        src="{{ url($org->logo) }}">
                                                </div>

                                            </td>
                                            <td class="border-b dark:border-dark-5">{{ $org->name }}</td>
                                            <td class="border-b dark:border-dark-5">{{ $org->phone }}</td>
                                            <td class="border-b dark:border-dark-5">{{ $org->email }}</td>
                                            <td class="border-b dark:border-dark-5">{{ $org->website }}</td>
                                            <td class="border-b dark:border-dark-5">
                                                <a class="btn btn-success mr-1 mb-2 "
                                                    href="{{ url('organization/' . $org->id . '/person') }}"> <i
                                                        data-feather="user" class="w-5 h-5"></i> </a>
                                                @if ($user->role == 'admin' || ($user->role == 'account_manager' && $user->organization_id == $org->id))
                                                    <a class="btn btn-primary mr-1 mb-2 modal-edit" data-toggle="modal"
                                                        data-target="#programmatically-modal"
                                                        wire:click="isEdit({{ $org->id }})"> <i
                                                            data-feather="edit-2" class="w-5 h-5"></i> </a>

                                                    <a class="btn btn-danger mr-1 mb-2 data-toggle=" modal"
                                                        wire:click="deleteData({{ $org->id }})"
                                                        data-target="#delete-modal-preview""> <i data-feather=" trash"
                                                        class="w-5 h-5"></i> </a>
                                                @endif
                                            </td>
                                        </tr>

                                    @empty
                                        <td colspan="{{ count($header) }}"
                                            class="border-b dark:border-dark-5 text-center">Data
                                            Empty</td>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="delete-modal-preview" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="p-5 text-center"> <i data-feather="x-circle"
                            class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Are you sure?</div>
                        <div class="text-gray-600 mt-2">Do you really want to delete these records? <br>This process
                            cannot be undone.</div>
                    </div>
                    <div class="px-5 pb-8 text-center"> <button type="button" data-dismiss="modal"
                            class="btn btn-outline-secondary w-24 dark:border-dark-5 dark:text-gray-300 mr-1">Cancel</button>
                        <button type="button" class="btn btn-danger w-24">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END: Modal Content -->
    <div id="programmatically-modal" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="font-medium text-base mr-auto">Create New Organization</h2>
                </div> <!-- END: Modal Header -->
                <div class="modal-body p-10 ">
                    <form wire:submit.prevent="saveModule(Object.fromEntries(new FormData($event.target)))"
                        id="saveModule" x-data="{ isUploading: false, progress: 0 }"
                        x-on:livewire-upload-start="isUploading = true"
                        x-on:livewire-upload-finish="isUploading = false"
                        x-on:livewire-upload-error="isUploading = false"
                        x-on:livewire-upload-progress="progress = $event.detail.progress">
                        <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-name" class="form-label">Name</label>
                                <input id="modal-name" type="text" class="form-control" wire:model="name" name="name"
                                    placeholder="Name">
                                @error('name') <span class="error">{{ $message }}</span> @enderror


                            </div>

                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-phone" class="form-label">Phone</label>
                                <input id="modal-phone" type="text" class="form-control" wire:model="phone"
                                    name="phone" placeholder="+62852xxxxx">
                                @error('phone') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-email" class="form-label">Email</label>
                                <input id="modal-email" type="text" class="form-control" wire:model="email"
                                    name="email" placeholder="example@gmail.com">
                                @error('email') <span class="error">{{ $message }}</span> @enderror

                            </div>
                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-website" class="form-label">Website</label>
                                <input id="modal-website" type="text" class="form-control" wire:model="website"
                                    name="website" placeholder="https://website.tld">
                            </div>

                            <div class="col-span-12 sm:col-span-6">
                                <label for="modal-logo" class="form-label">Logo</label>
                                <input id="modal-logo" type="file" wire:model="logo" class="form-control" name="logo">
                                @error('logo') <span class="error">{{ $message }}</span> @enderror

                            </div>


                        </div> <!-- END: Modal Body -->
                        <!-- BEGIN: Modal Footer -->
                        <div class="modal-footer text-right"> <button type="button" data-dismiss="modal"
                                class="btn btn-outline-secondary w-20 mr-1">Cancel</button> <button type="submit"
                                class="btn btn-primary w-20">Send</button> </div> <!-- END: Modal Footer -->
                    </form>
                </div>
            </div>
        </div>
    </div> <!-- END: Modal Content -->
</div>
@section('script')
    <script src="https://code.jquery.com/jquery-3.6.0.slim.js"
        integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>

    <script>
        // Show modal
        cash('#programmatically-show-modal').on('click', function() {
            cash('#programmatically-modal').modal('show')
        }) // Hide modal
        cash('#programmatically-hide-modal').on('click', function() {
            cash('#programmatically-modal').modal('hide')
        }) // Toggle modal
        cash('#programmatically-toggle-modal').on('click', function() {
            cash('#programmatically-modal').modal('toggle')
        })
        window.addEventListener('getId', event => {
            console.log(event.detail)

            if (event.detail.isEdit) {

                cash("#header-footer-modal-preview").modal("show");
                // $('#confirm-delete-catalog').modal('show')
            }
        })
        // $('.modal-edit').click(function() {
        //     cash("#header-footer-modal-preview").modal("show");
        // })
    </script>
    @include('livewire.include.script')
@endsection
