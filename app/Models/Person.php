<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    protected $table = 'person';

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id','id');
    }
}
