<?php

namespace App\Http\Livewire;

use App\Models\Organization;
use App\Models\Person;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Ramsey\Uuid\Uuid;

class PersonComp extends Component
{
    use WithPagination;
    use WithFileUploads;
    public $search;
    public $slug;
    public $name;
    public $email;
    public $ids;
    public $website;
    public $fileSelected;
    public $newFile;
    public $avatar;
    public $isEdit;
    public $logoFile;
    public $logoData = [];
    public $phone;
    protected $rules = [
        'avatar' => 'required|mimes:jpg,jpeg,png',
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'website' => 'required',
    ];
    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1],
    ];

    protected $listeners = [
        'upload:finished' => 'fileSelected',
        'modalHidden' => 'resetValue'
    ];

    public function mount($slug)

    {
        $this->slug = $slug;
    }

    public function query()
    {
        $data = Person::query()->where('name', 'like', '%' . $this->search . '%')->where('organization_id', $this->slug);
        return $data = $data->paginate(10);
    }

    public function tableHead()
    {
        return [
            'avatar', 'name', 'email', 'phone', 'action'
        ];
    }

    public function render()
    {
        $organization = Organization::query()->find($this->slug);
        return view('livewire.person-comp', [
            'data' => $this->query(),
            'header' => $this->tableHead(),
            'organization' => $organization,
            'user' => Auth::user()
        ]);
    }
    public function fileSelected()
    {
        $q = $this->avatar;
        $mime = explode('/', $q->getMimeType())[0];
        $this->newFile = true;
        $this->logoData = [
            'name' => $q->getClientOriginalName(),
            'file_path' =>  $q->temporaryUrl(),
            'filetype' => $q->getClientOriginalExtension(),
        ];
    }

    private function uploadFIle(): array
    {
        $id = Uuid::uuid4();
        $path = '/public/uploads/person/';
        $file = $id . '.' . $this->avatar->getClientOriginalExtension();
        $this->avatar->storeAs($path, $file, $disk = 'local');
        $this->logoFile = (str_replace('/public', '', $path) . $file);
        return [
            'name' => $this->avatar->getClientOriginalName(),
            'file_path' => (str_replace('/public', '', $path) . $file),
            'filetype' => $this->avatar->getClientOriginalExtension()
        ];
    }

    public function isEdit($id = null)
    {
        if ($id == null) {
            $this->isEdit = false;
        } else {
            $this->isEdit = true;

            $data = Person::query()->find($id);

            $this->ids = $data->id;
            $this->name = $data->name;
            $this->email = $data->email;
            $this->phone = $data->phone;
            $this->avatar = $data->avatar;

            $this->dispatchBrowserEvent('getId', [
                'isEdit' => $this->isEdit,
                'data' => $data
            ]);
        }
    }
    public function saveModule($data)
    {
        // $this->validate();
        if ($this->newFile) {
            $this->uploadFIle();
        }

        $save = !$this->isEdit ? new Person() : Person::query()->find($this->ids);
        $save->organization_id = $this->slug;
        $save->name = $this->name;
        $save->email = $this->email;
        $save->phone = $this->phone;
        if ($this->newFile) {
            $save->avatar = $this->logoFile;
        }
        $save->save();

        $type = !$this->isEdit ? 'Update' : 'Save';


        $this->dispatchBrowserEvent('actResponse', [
            'success' => true,
            'message' => 'Success ' . $type . ' data',
            'isModal' => true,
            'modalName' => 'header-footer-modal-preview',
            'hideModal' => true
        ]);
    }

    public function deleteData($id)
    {
        $find = Person::query()->find($id);
        if (!$find) {
            $this->dispatchBrowserEvent('actResponse', [
                'success' => false,
                'message' => 'Organization not found',
                'isModal' => true,
                'modalName' => 'confirm-delete-catalog',
                'hideModal' => true
            ]);
        }

        $find->delete();

        $this->dispatchBrowserEvent('actResponse', [
            'success' => true,
            'message' => 'Deleted',
            'isModal' => true,
            'modalName' => 'delete-modal-preview',
            'hideModal' => true
        ]);
    }
}
