<?php

namespace App\Http\Livewire;

use App\Models\Organization;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Ramsey\Uuid\Uuid;

class UserComp extends Component
{
    use WithPagination;
    use WithFileUploads;
    public $search;
    public $slug;
    public $name;
    public $email;
    public $ids;
    public $role;
    public $fileSelected;
    public $newFile;
    public $organization_id;
    public $isEdit;
    public $logoFile;
    public $logoData = [];
    public $phone;
    protected $rules = [
        // 'role' => 'required',
    ];
    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1],
    ];

    protected $listeners = [
        'upload:finished' => 'fileSelected',
        'modalHidden' => 'resetValue'
    ];

    public function mount()

    {
    }

    public function query()
    {
        $data = User::query()->where('users.name', 'like', '%' . $this->search . '%')->leftJoin('organizations', 'users.organization_id', 'organizations.id')->select('users.*', 'organizations.name as organization');
        return $data = $data->paginate(10);
    }

    public function tableHead()
    {
        return [
            'name', 'email', 'role', 'organization', 'action'
        ];
    }

    public function render()
    {
        $org = Organization::query()->get();
        return view('livewire.user-comp', [
            'data' => $this->query(),
            'header' => $this->tableHead(),
            'organization' => $org
        ]);
    }
    public function fileSelected()
    {
        $q = $this->avatar;
        $mime = explode('/', $q->getMimeType())[0];
        $this->newFile = true;
        $this->logoData = [
            'name' => $q->getClientOriginalName(),
            'file_path' =>  $q->temporaryUrl(),
            'filetype' => $q->getClientOriginalExtension(),
        ];
    }

    private function uploadFIle(): array
    {
        $id = Uuid::uuid4();
        $path = '/public/uploads/person/';
        $file = $id . '.' . $this->avatar->getClientOriginalExtension();
        $this->avatar->storeAs($path, $file, $disk = 'local');
        $this->logoFile = (str_replace('/public', '', $path) . $file);
        return [
            'name' => $this->avatar->getClientOriginalName(),
            'file_path' => (str_replace('/public', '', $path) . $file),
            'filetype' => $this->avatar->getClientOriginalExtension()
        ];
    }

    public function isEdit($id = null)
    {
        if ($id == null) {
            $this->isEdit = false;
        } else {
            $this->isEdit = true;

            $data = User::query()->find($id);

            $this->ids = $data->id;
            $this->name = $data->name;
            $this->role = $data->role;
            $this->organization_id = $data->organization_id;

            $this->dispatchBrowserEvent('getId', [
                'isEdit' => $this->isEdit,
                'data' => $data
            ]);
        }
    }
    public function saveModule($data)
    {
        // $this->validate();
        if ($this->newFile) {
            $this->uploadFIle();
        }

        $save = User::query()->find($this->ids);
        $save->role = $this->role;
        $save->organization_id = $this->organization_id;

        $save->save();

        $type = !$this->isEdit ? 'Update' : 'Save';


        $this->dispatchBrowserEvent('actResponse', [
            'success' => true,
            'message' => 'Success ' . $type . ' data',
            'isModal' => true,
            'modalName' => 'header-footer-modal-preview',
            'hideModal' => true
        ]);
    }

    public function deleteData()
    {
        $find = User::query()->find($this->ids);
        if (!$find) {
            $this->dispatchBrowserEvent('actResponse', [
                'success' => false,
                'message' => 'Organization not found',
                'isModal' => true,
                'modalName' => 'confirm-delete-catalog',
                'hideModal' => true
            ]);
        }
    }
}
