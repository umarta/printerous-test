<?php

namespace App\Http\Livewire;

use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Ramsey\Uuid\Uuid;

class OrganizationComp extends Component
{
    use WithPagination;
    use WithFileUploads;
    public $search;

    public $organizationList = [];
    public $name;
    public $email;
    public $ids;
    public $website;
    public $fileSelected;
    public $newFile;
    public $logo;
    public $isEdit;
    public $logoFile;
    public $logoData = [];
    public $phone;
    protected $rules = [
        'logo' => 'required|mimes:jpg,jpeg,png',
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'website' => 'required',
    ];
    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1],
    ];

    protected $listeners = [
        'upload:finished' => 'fileSelected',
        'modalHidden' => 'resetValue'
    ];

    public function mount()
    {
    }

    public function query()
    {
        $data = Organization::query()->where('name', 'like', '%' . $this->search . '%');
        return $data = $data->paginate(10);
    }

    public function tableHead()
    {
        return [
            'logo', 'name', 'phone', 'email', 'website', 'action'
        ];
    }

    public function render()
    {
        return view('livewire.organization-comp', [
            'data' => $this->query(),
            'header' => $this->tableHead(),
            'user' => Auth::user()
        ]);
    }
    public function fileSelected()
    {
        $q = $this->logo;
        $mime = explode('/', $q->getMimeType())[0];
        $this->newFile = true;
        $this->logoData = [
            'name' => $q->getClientOriginalName(),
            'file_path' =>  $q->temporaryUrl(),
            'filetype' => $q->getClientOriginalExtension(),
        ];
    }

    private function uploadFIle(): array
    {
        $id = Uuid::uuid4();
        $path = '/public/uploads/organization/';
        $file = $id . '.' . $this->logo->getClientOriginalExtension();
        $this->logo->storeAs($path, $file, $disk = 'local');
        $this->logoFile = (str_replace('/public', '', $path) . $file);
        return [
            'name' => $this->logo->getClientOriginalName(),
            'file_path' => (str_replace('/public', '', $path) . $file),
            'filetype' => $this->logo->getClientOriginalExtension()
        ];
    }

    public function isEdit($id = null)
    {
        if ($id == null) {
            $this->isEdit = false;
        } else {
            $this->isEdit = true;

            $data = Organization::query()->find($id);

            $this->ids = $data->id;
            $this->name = $data->name;
            $this->email = $data->email;
            $this->website = $data->website;
            $this->phone = $data->phone;
            $this->logo = $data->logo;

            $this->dispatchBrowserEvent('getId', [
                'isEdit' => $this->isEdit,
                'data' => $data
            ]);
        }
    }
    public function saveModule($data)
    {
        // $this->validate();
        if ($this->newFile) {
            $this->uploadFIle();
        }

        $save = !$this->isEdit ? new Organization() : Organization::query()->find($this->ids);
        $save->name = $this->name;
        $save->email = $this->email;
        $save->website = $this->website;
        $save->phone = $this->phone;
        if ($this->newFile) {
            $save->logo = $this->logoFile;
        }
        $save->save();

        $type = !$this->isEdit ? 'Update' : 'Save';


        $this->dispatchBrowserEvent('actResponse', [
            'success' => true,
            'message' => 'Success ' . $type . ' data',
            'isModal' => true,
            'modalName' => 'header-footer-modal-preview',
            'hideModal' => true
        ]);
    }

    public function deleteData($id)
    {
        $find = Organization::query()->find($id);
        if (!$find) {
            $this->dispatchBrowserEvent('actResponse', [
                'success' => false,
                'message' => 'Organization not found',
                'isModal' => true,
                'modalName' => 'confirm-delete-catalog',
                'hideModal' => true
            ]);
        }
        $find->delete();

        $this->dispatchBrowserEvent('actResponse', [
            'success' => true,
            'message' => 'Deleted',
            'isModal' => true,
            'modalName' => 'delete-modal-preview',
            'hideModal' => true
        ]);
    }
}
